# Elmcreek Better Farms
## Makes more buildings new farmer only, allowing for more customization of farms in farm manager and farm owner gamemode.
### Screenshots
![Screenshot 1](https://gitlab.com/minecraftchest1/my-fs22-mods/-/raw/main/Elmcreek-Better-Farms/Screenshots/Farming_Simulator_22_12_20_2021_9_29_16_AM.png)
![Screenshot 2](https://gitlab.com/minecraftchest1/my-fs22-mods/-/raw/main/Elmcreek-Better-Farms/Screenshots/Farming_Simulator_22_12_20_2021_9_28_45_AM.png)
![Screenshot 3](https://gitlab.com/minecraftchest1/my-fs22-mods/-/raw/main/Elmcreek-Better-Farms/Screenshots/Farming_Simulator_22_12_20_2021_9_29_41_AM.png)
![Screenshot 4](https://gitlab.com/minecraftchest1/my-fs22-mods/-/raw/main/Elmcreek-Better-Farms/Screenshots/Farming_Simulator_22_12_20_2021_9_32_55_AM.png)

## Installation
Look for this mod on modhub. ALl releases are avaible in the releases folder. 

## Support
You can file issues using the Gitlab issues tracker. If you do not have a Gitlab account and do not want to create one, you can also send an email to project+minecraftchest1-my-fs22-mods-32296630-issue-@incoming.gitlab.com.'

## Chat

| Service | Link |
| ------ | ------ |
| Matrix | https://matrix.to/#/#mc1-fs22-mods:matrix.org |
| Discord | https://discord.gg/bbNmSKSFH5 |

## Social

Follow the project on Mastodon at @minecraftchest1_fsmods@mastodon.social
