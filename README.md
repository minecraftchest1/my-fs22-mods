# My-FS22-mods
## Issue tracker for my Farming Simulator 22 mods.

## Screenshots
Screenshots will be in each mods folder in this repro.

## Installation
I will attempt to publish all of my mods to the Farming Simulator modhub. I will also put all versions my mods in this repro.

## Support
You can file issues using the Gitlab issues tracker. If you do not have a Gitlab account and do not want to create one, you can also send an email to project+minecraftchest1-my-fs22-mods-32296630-issue-@incoming.gitlab.com.'

## Chat

| Service | Link |
| ------ | ------ |
| Matrix | https://matrix.to/#/#mc1-fs22-mods:matrix.org |
| Discord | https://discord.gg/bbNmSKSFH5 |

## Social
Follow the project on Mastodon at [@minecraftchest1_fsmods@mastodon.social](https://mastodon.social/web/@minecraftchest1_fsmods)
